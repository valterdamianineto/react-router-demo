import { useParams } from "react-router-dom"

interface Props {

}

const Params = (props: Props) => {
    const { id } = useParams()

    return (
        <div>
            <h1>Params</h1>
            <p>Valor: ${id}</p>
        </div>
    )
}

export default Params