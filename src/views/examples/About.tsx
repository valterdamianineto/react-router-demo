interface Props {

}

const About = (props: Props) => (
    <div>
        <h1>About</h1>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsam odio dolore magni explicabo? Unde exercitationem voluptates animi, quibusdam fuga inventore neque aperiam esse quod quidem amet eos non tempore doloribus?</p>
    </div>
)

export default About;