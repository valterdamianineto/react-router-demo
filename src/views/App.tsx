import { BrowserRouter } from 'react-router-dom'; // essa importação mudou
import Content from '../components/layout/content/Content';
import Menu from '../components/layout/menu/Menu';

import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter> 
          <Menu />
          <Content />
      </BrowserRouter>
    </div>
  );
}

export default App;
