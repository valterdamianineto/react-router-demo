import { Route, Routes } from "react-router-dom";
import About from "../../../views/examples/About";
import Home from "../../../views/examples/Home";
import NotFound from "../../../views/examples/NotFound";
import Param from "../../../views/examples/Params";
import './Content.css';

interface Props {
}

const Content = (props: Props) => (
    <main className='content'>
        <Routes>
            <Route path="/"  element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/param/:id" element={<Param />} />
            <Route path="*" element={<NotFound />} />
        </Routes>
    </main>
)

export default Content;
