# React Router

## Project

The project demonstrates how react router works, showing the navigation structure, the use of fixed routes and the use of variable routes.

##  Technologies

-  [JavaScript](https://www.javascript.com/)
-  [React](https://pt-br.reactjs.org/)
-  [Typescript](https://www.typescriptlang.org/)
-  [React Router](https://reactrouter.com/en/main)


## 📥 Installation and execution

Clone this repository.

```bash
# To run this project it is necessary to have Node installed, as well as Yarn or NPM

# Installing dependencies
$ npm i

# Running application
$ npm start

# The system default execution port is:
$ http://localhost:3000/
